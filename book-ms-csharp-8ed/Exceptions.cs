using System;

namespace Cs8ed
{
    internal static class Exceptions
    {
        public static void WithFilters(string exceptionMessage)
        {
            void ThrowException() => throw new Exception("YEAH");

            try
            {
                ThrowException();
            }
            catch (Exception e) when (e.Message == exceptionMessage)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}