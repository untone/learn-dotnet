﻿using System;

namespace Cs8ed.Definitions
{
    interface IInterfaceA
    {
        void Do();
    }
    interface IInterfaceB
    {
        void Do();
    }
    interface IInterfaceC : IInterfaceB
    {
        void Do();
    }

    class ClassA : IInterfaceA, IInterfaceB
    {
        void IInterfaceA.Do()
        {
            throw new NotImplementedException();
        }

        void IInterfaceB.Do()
        {
            throw new NotImplementedException();
        }
    }
    class ClassB : IInterfaceA, IInterfaceB, IDisposable
    {
        private bool disposed = false;
        public void Do()
        {
        }
        ~ClassB()
        {
            this.Dispose(false);
        }
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            // lock ?
            if (!this.disposed)
            {
                if (disposing)
                {
                    // release managed resources
                }
                // release unmanaged resources
                this.disposed = true;
            }
        }
    }
}