﻿using System.Collections.Generic;

namespace Cs8ed.Definitions
{
    public class Members
    {
        public int PropInt1 { get; }
        public int PropInt2 { get; } = 199;

        Dictionary<string, int> ages1 = new Dictionary<string, int>
        {
            ["item1"] = 4,
            ["item2"] = 66
        };
        Dictionary<string, int> ages2 = new Dictionary<string, int>
        {
            {"item1", 4},
            {"item2", 66}
        };

        public Members()
        {
            PropInt1 = 99;
        }

        public void CheckParams(int left, int right)    // higher priority
        {
        }

        public void CheckParams(params int[] prms)
        {
        }
    }
}