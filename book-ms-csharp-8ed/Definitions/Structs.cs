﻿namespace Cs8ed.Definitions
{
    interface ITime { }

    struct AnotherStruct
    {
        public int val1;
        public int val2;
        //public int val3 = 0; // error

        public AnotherStruct(int inVal1)
        {
            val1 = inVal1;
            val2 = 4;
        }
    }

    struct TimeStruct : ITime
    {
        private int hours, minutes, seconds;
        //public TimeStruct() { }
        public TimeStruct(int hours)
        {
            this.hours = hours;
            this.minutes = 0;
            this.seconds = 0;
        }
        //~TimeStruct(){}

        public static TimeStruct operator ++(TimeStruct ts)
        {
            ts.hours++;
            return ts;
        }

        // + operator
        public static TimeStruct operator +(TimeStruct left, TimeStruct right)
        {
            return Add(left, right);
        }
        public static TimeStruct operator +(TimeStruct left, int right)
        {
            return Add(left, right);
        }
        public static TimeStruct operator +(int left, TimeStruct right)
        {
            return Add(left, right);
        }
        // for other languages compatibility:
        public static TimeStruct Add(TimeStruct left, TimeStruct right)
        {
            return new TimeStruct(left.hours + right.hours);
        }
        public static TimeStruct Add(TimeStruct left, int right)
        {
            return new TimeStruct(left.hours + right);
        }
        public static TimeStruct Add(int left, TimeStruct right)
        {
            return new TimeStruct(left + right.hours);
        }

        // == operator
        public static bool operator ==(TimeStruct t1, TimeStruct t2)
        {
            return t1.hours == t2.hours;
        }
        // required if == operator defined
        public static bool operator !=(TimeStruct t1, TimeStruct t2)
        {
            return t1.hours != t2.hours;
        }
        // must be overriden if == and != defined
        public override int GetHashCode()
        {
            return hours.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return hours.Equals(((TimeStruct)obj).hours);
        }

        // casting
        public static implicit operator int(TimeStruct t)
        {
            return t.hours;
        }

        // another way starts working for operator if having implicit casting operator
        public static TimeStruct operator -(TimeStruct t1, TimeStruct t2)
        {
            return new TimeStruct(t1.hours - t2.hours);
        }
        public static implicit operator TimeStruct(int i)
        {
            return new TimeStruct(i);
        }
    }
}
