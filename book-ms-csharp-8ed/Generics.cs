namespace Cs8ed
{
    static internal class Generics
    {
        public static void CovarianceContravariance()
        {
            var stringWrapper = new Wrapper<string>();
            IRetrieveWrapper<object> retrieveObjectWrapper = stringWrapper;
            var intWrapper = new Wrapper<int>();
            //retrieveObjectWrapper = intWrapper;

            var objectWrapper = new Wrapper<object>();
            IStoreWrapper<string> storeStringWrapper = objectWrapper;
            objectWrapper.SetData("");
        }
    }

    interface IStoreWrapper<in T>   // contravariance
    {
        void SetData(T data);
    }
    
    interface IRetrieveWrapper<out T> // covariance
    {
        T GetData();
    }
    
    class Wrapper<T> : IStoreWrapper<T>, IRetrieveWrapper<T>
    {
        private T storedData;
        public void SetData(T data)
        {
            storedData = data;
        }
        public T GetData()
        {
            return storedData;
        }
    }
}