namespace Cs8ed
{
    internal static class Unsafe
    {
        public static void Unsafe2()
        {
            int x = 99, y = 100;
            unsafe
            {
                UnsafeSwap(&x, &y);
            }
        }

        static unsafe void UnsafeSwap(int* a, int* b)
        {
            int tmp = *a;
            *a = *b;
            *b = tmp;
        }
    }
}