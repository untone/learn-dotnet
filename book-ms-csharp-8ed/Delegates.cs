using System;

namespace Cs8ed
{
    static internal class Delegates
    {
        public static void UnnamedMethod()
        {
            CallUnnamedMethod(delegate { Console.WriteLine("delegatettt"); });
            CallUnnamedMethod2(delegate { Console.WriteLine("delegatettt"); });
            CallUnnamedMethod2(delegate (string s, int i) { Console.WriteLine("delegatettt" + s + i); });
        }

        static UnnamedMethodDelegate delg0;

        public static void UseDelegate()
        {
            UnnamedMethodDelegate delg = null;
            UnnamedMethodDelegate delg1 = new UnnamedMethodDelegate(DataStructures.ListFind);
            delg += DataStructures.ListFind;
            delg0 += DataStructures.ListFind;
            CallUnnamedMethod(delg);
            CallUnnamedMethod(delg0);
            CallUnnamedMethod(delg1);
        }

        delegate void UnnamedMethodDelegate();
        delegate void UnnamedMethodDelegate2(string s, int i);

        private static void CallUnnamedMethod(UnnamedMethodDelegate method)
        {
            method();
        }
        private static void CallUnnamedMethod2(UnnamedMethodDelegate2 method)
        {
            method("dd", 33);
        }
    }
}