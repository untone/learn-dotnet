﻿using System;

namespace Cs8ed
{
    class Program
    {
        static void Main(string[] args)
        {
            Numbers.FloatZeroNanInfinit();
            Numbers.Overflows();

            Conditions.SwitchLabels(7);
            Exceptions.WithFilters("YEAH");

            Unsafe.Unsafe2();

            DataStructures.DoArrays();
            DataStructures.ListFind();

            Types.CreateStruct();
            Types.StructWithoutConstructor();
            Types.CreateClassInstance();

            References.PassByIndexer();
            Operators.AdditiveOperator();

            Generics.CovarianceContravariance();

            StandardBehaviors.SortComparable();

            Delegates.UnnamedMethod();
            Delegates.UseDelegate();

            Tasks.SimpleTasks();
            Tasks.SimpleParallel();
            Tasks.CancellingTaskOuter();
            Tasks.CancellingTaskInner();
            Tasks.NewIAsyncResult();
            
            Paralleling.RunAsParallel();
            Paralleling.UseSyncPrimitives();

            AsyncAwait.SimpleAsyncAwait();
            AsyncAwait.ResultAsyncAwait();

            Console.WriteLine("\n\nFinished");
            Console.ReadKey();
        }
    }
}
