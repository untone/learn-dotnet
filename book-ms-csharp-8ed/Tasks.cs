﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cs8ed
{
    public static class Tasks
    {
        public static void SimpleTasks()
        {
            Task task1 = new Task(DoTask1);
            task1.Start();
            Task task2 = Task.Run(DoTask1);

            Task.WaitAll(task1, task2);

            Task task3 = Task.Run(DoTask1)
                .ContinueWith(ContinueWithTask2);
            task3.Wait();

            Task task4 = new Task(DoTask1);
            task4.ContinueWith(ContinueWithTask2, TaskContinuationOptions.OnlyOnRanToCompletion);
            task4.Start();

            Task.WaitAny(task3, task4);
        }

        public static void CancellingTaskOuter()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken cancellationToken = cts.Token;
            cancellationToken.Register(() =>
            {
                Console.WriteLine("\npost-cancel");
            });
            Task task = Task.Run(() => CancelableTaskOuter(cancellationToken), cancellationToken); // passing cancellationToken for letting CLR know that task was cancelled, otherwise task.Status will be not Cancelled

            Thread.Sleep(200);
            Thread.SpinWait(200);

            if (task.Status == TaskStatus.Running)
            {
                Console.WriteLine("tasks running");
            }
            cts.Cancel();
            try
            {
                task.Wait();
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (AggregateException ae)
            {
                Console.WriteLine(ae.Message);
                ae.Handle(e =>
                {
                    return e is OperationCanceledException; // actually here TaskCanceledException (based on OperationCanceledException). OperationCanceledException would be if 'await' was used
                });
            }
            finally
            {
                Console.WriteLine($"task status {task.Status}");    // cancelled
                cts.Dispose();
            }
        }

        public static void CancellingTaskInner()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            Task task = Task.Run(() => CancelableTaskInner(cts), cts.Token); // passing cancellationToken for letting CLR know that task was cancelled, otherwise task.Status will be not Cancelled

            Thread.Sleep(200);
            Thread.SpinWait(200);

            try
            {
                task.Wait(cts.Token);
            }
            catch (OperationCanceledException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (AggregateException ae)
            {
                Console.WriteLine(ae.Message);
                ae.Handle(e => e is OperationCanceledException);
            }
            finally
            {
                Console.WriteLine($"task status {task.Status}"); // ranToComplete
                cts.Dispose();
            }
        }

        public static void SimpleParallel()
        {
            Parallel.Invoke(DoTask1, DoTask2);

            Console.WriteLine();
            Parallel.For(5, 30, OutputValue);

            Console.WriteLine();
            Parallel.ForEach(Enumerable.Range(3, 14), (i, state) =>
            {
                if (i >= 10)
                {
                    state.Stop();
                }
                OutputValue(i);
            });
        }

        public static async void NewIAsyncResult()
        {
            byte[] b = Encoding.UTF8.GetBytes("hello");
            MemoryStream s = new MemoryStream();
            await Task.Factory.FromAsync(s.BeginWrite, s.EndWrite, b, 0, b.Length, null);
            Console.WriteLine(Encoding.UTF8.GetString(s.GetBuffer()));
        }

        private static void CancelableTaskOuter(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                Thread.Sleep(300);
            }
            cancellationToken.ThrowIfCancellationRequested();
        }

        private static void CancelableTaskInner(CancellationTokenSource cancellationTokenSource)
        {
            Thread.Sleep(400);
            cancellationTokenSource.Cancel();
        }

        private static void DoTask1()
        {
            Thread.Sleep(1000);
        }
        private static void DoTask2()
        {
            Thread.Sleep(500);
        }

        private static void OutputValue(int val)
        {
            Console.Write(val + " ");
        }

        private static void ContinueWithTask2(Task task)
        {
            DoTask2();
        }
    }
}