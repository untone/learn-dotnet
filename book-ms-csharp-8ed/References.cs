namespace Cs8ed
{
    internal static class References
    {
        public static void PassByIndexer()
        {
            int[] pins = { 9, 3, 7, 2 };
            RefMethod(ref pins[2]);

            var indexer = new IndexerClassA();
            //RefMethod(ref indexer["sdsd"]);
        }

        public static void RefMethod(ref int val)
        {
            if (val > 5)
                val = 5;
        }
    }
}