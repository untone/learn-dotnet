using System;

namespace Cs8ed
{
    internal static class Conditions
    {
        public static void SwitchLabels(int v)
        {
            switch (v)
            {
                case 1:
                    Console.WriteLine(1);
                    break;
                case 3:
                    Console.WriteLine(3);
                    break;
                case 5:
                    Console.WriteLine(5);
                    break;
                case 7:
                    Console.WriteLine(7);
                    goto case 3;
                default:
                    Console.WriteLine(0);
                    break;
            }
        }
    }
}