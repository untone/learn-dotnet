using Cs8ed.Definitions;

namespace Cs8ed
{
    internal static class Operators
    {
        public static void AdditiveOperator()
        {
            TimeStruct t1 = new TimeStruct(2);
            TimeStruct t2 = new TimeStruct(3);
            TimeStruct tt = t1 + t2;
            tt = tt + 5;
            tt = 5 + tt;
            tt += t1;
            tt += 5;
            tt++;
            tt--; // double casting
            int i = tt;
            tt = tt - 5;
        }
    }
}