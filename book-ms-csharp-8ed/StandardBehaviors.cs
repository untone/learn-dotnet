﻿using System;
using System.Collections.Generic;

namespace Cs8ed
{
    public static class StandardBehaviors
    {
        class MyClass1 : IComparable<MyClass1>
        {
            static int cnt = 0;
            private int val = ++cnt;
            public int CompareTo(MyClass1 other)
            {
                return this.val.CompareTo(other.val);
            }
        }


        public static void SortComparable()
        {
            var c1 = new MyClass1();
            var c2 = new MyClass1();
            var list = new List<MyClass1> {c2, c1};
            list.Sort();
        }
    }
}