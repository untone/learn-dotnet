﻿namespace Cs8ed
{
    internal class IndexerClassA
    {
        public string this[int index]
        {
            get => index.ToString();
            set
            {
                // do something
            }
        }

        public int this[IndexerClassA index] => index.GetHashCode();

        public IndexerClassA this[string index]
        {
            set
            {
                // do something
            }
        }
    }
}