using System.Collections.Generic;
using System.Linq;

namespace Cs8ed
{
    internal static class DataStructures
    {
        public static void DoArrays()
        {
            int[] pins = { 9, 3, 7, 2 };
            int[] copy = new int[pins.Length];
            pins.CopyTo(copy, 0);
            int[] clone = (int[])pins.Clone();

            int[,] items = new int[4, 6];
            items[2, 3] = 99;
            int[][] items2 = new int[4][];
            items2[0] = new int[2];
            items2[1] = new int[5];
        }

        public static void ListFind()
        {
            var list = new List<string> { "iotem1", "item2", "item3" };
            List<string> t1 = list.FindAll(x => x.StartsWith("item"));
            IEnumerable<string> t2 = list.Where(x => !x.StartsWith("item"));
        }
    }
}