﻿using System.Linq;
using System.Threading;

namespace Cs8ed
{
    public static class Paralleling
    {
        public static void RunAsParallel()
        {
            var clist = Enumerable.Range(1, 100)
                .Select(x => new { Number = x, Name = "n" + x }).ToList();
            var olist = Enumerable.Range(21, 30)
                .Select(x => new { Number = x, Caller = "c" + x }).ToList();

            var t1 = clist.AsParallel()
                .Join(olist.AsParallel(), c => c.Number, o => o.Number,
                    (c, o) => new { c.Number, c.Name, o.Caller }).ToList();
            //foreach (var t in t1)
            //{
            //    Console.WriteLine(t);
            //}
            
            CancellationTokenSource cts = new CancellationTokenSource();
            var t2 = clist.AsParallel().WithCancellation(cts.Token)
                .Join(olist.AsParallel(), c => c.Number, o => o.Number,
                    (c, o) => new { c.Number, c.Name, o.Caller }).ToList();
            cts.Cancel();
            //foreach (var t in t1)
            //{
            //    Console.WriteLine(t);
            //}
        }

        public static void UseSyncPrimitives()
        {
            var semaphore = new SemaphoreSlim(2, 2);
            semaphore.Wait();
            // do something
            semaphore.Release();
            semaphore.Dispose();

            var manualResetEvent = new ManualResetEventSlim(true);
            //manualResetEvent.Wait(cancellationToken);
            manualResetEvent.Wait();
            manualResetEvent.Set();
            manualResetEvent.Reset();
            manualResetEvent.Dispose();

            var countdownEvent = new CountdownEvent(2);
            countdownEvent.Signal();
            countdownEvent.Signal();
            countdownEvent.Wait();
            countdownEvent.Reset();
            countdownEvent.AddCount();
            countdownEvent.Dispose();

            var readerWriterLock = new ReaderWriterLockSlim();
            readerWriterLock.EnterReadLock();
            readerWriterLock.ExitReadLock();
            readerWriterLock.EnterWriteLock();
            readerWriterLock.ExitWriteLock();
            readerWriterLock.EnterUpgradeableReadLock();
            readerWriterLock.EnterWriteLock();
            readerWriterLock.ExitWriteLock();
            readerWriterLock.ExitUpgradeableReadLock();

            var barrier = new Barrier(2);
            barrier.AddParticipant();
            barrier.RemoveParticipants(2);
            barrier.SignalAndWait();
            barrier.Dispose();
        }
    }
}