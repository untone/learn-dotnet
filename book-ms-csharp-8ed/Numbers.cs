using System;

namespace Cs8ed
{
    internal static class Numbers
    {
        public static void FloatZeroNanInfinit()
        {
            double db1 = 5.0;
            double db0 = 0.0;
            var inf = db1 / db0;
            var nan = db0 / db0;
            nan = 10 + nan;
            inf = 10 + inf;
            nan = inf * 0;
            nan = nan + inf;
        }

        public static void Overflows()
        {
            int number = Int32.MaxValue;
            int notThrow = number + 1;
            try
            {
                checked
                {
                    int willThrow = number + 1;
                }
            }
            catch { }
            try
            {
                int willThrow = checked(number + 1);
            }
            catch { }

            unchecked
            {
                notThrow = number + 1;
            }
            notThrow = unchecked(number + 1);
        }
    }
}