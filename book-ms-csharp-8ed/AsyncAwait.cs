﻿using System;
using System.Threading.Tasks;

namespace Cs8ed
{
    public static class AsyncAwait
    {
        public static void SimpleAsyncAwait()
        {
            Task.Run(DoAsyncOps);
        }
        public static void ResultAsyncAwait()
        {
            Task<int> sumTask = Task.Run(DoAsyncOpsResult);
        }

        private static async Task DoAsyncOps()
        {
            await DoFirstOp();
            await DoSecondOp();
            await DoThirdOp();
            //return null;
        }

        private static async Task<int> DoAsyncOpsResult()
        {
            int sum = 0;
            sum += await DoFirstOpResult().ConfigureAwait(false);
            sum += await DoSecondOpResult();
            sum += await DoThirdOpResult().ConfigureAwait(false);
            Console.WriteLine(sum);
            return sum;
        }

        private static async Task DoFirstOp()
        {
            await Task.Delay(400);
            Console.WriteLine("1st op finished");
        }
        private static async Task DoSecondOp()
        {
            await Task.Delay(800);
            Console.WriteLine("2nd op finished");
        }
        private static async Task DoThirdOp()
        {
            await Task.Delay(1200);
            Console.WriteLine("3rd op finished");
        }

        private static async Task<int> DoFirstOpResult()
        {
            await Task.Delay(400);
            Console.WriteLine("1st op finished");
            return 400;
        }
        private static async Task<int> DoSecondOpResult()
        {
            await Task.Delay(800);
            Console.WriteLine("2nd op finished");
            return 800;
        }
        private static async Task<int> DoThirdOpResult()
        {
            await Task.Delay(1200);
            Console.WriteLine("3rd op finished");
            return 1200;
        }
    }
}