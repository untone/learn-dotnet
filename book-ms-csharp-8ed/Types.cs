﻿using System;
using Cs8ed.Definitions;

namespace Cs8ed
{
    public class Types
    {
        public static void CreateStruct()
        {
            var time = new TimeStruct();
            Console.Write(time);
        }

        public static void CreateClassInstance()
        {
            var c1 = new ClassA();
            var c2 = new ClassB();
        }

        public static void StructWithoutConstructor()
        {
            AnotherStruct ss;
            ss.val1 = 0;
            DateTime dt;
            //DateTime dt2 = dt; // error
        }
    }
}